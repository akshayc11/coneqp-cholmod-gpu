Authors:
Akshay Chandrashekaran
Suyoun Kim

This is the python script and the cholmod solver source code used for our project. 

It is a hacky solution with hardcoded paths.

Copy over the cholmod_test.c and Makefile to your <SUITESPARSE>/CHOLMOD/Demo/ folder.
You can modify the code to swithc between CPU and GPU mode by performing:

1) Open File in editor:

2) Search for (->useGpu)

3) Set to 1 to activate GPU.

Look at the bottom of the interior_point.py to modify the size of the problem being solved.
