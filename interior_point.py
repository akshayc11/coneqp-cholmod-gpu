# THis function is used to perform primal dual interior point methods for a conic problem
# of the form min  x'Px + c'x
#             s.t. Gx \preceq h
#                  Ax \leq b
# with P \in S^n_{++}

import numpy as np
import scipy as sp
from scipy.io import mmread, mmwrite, mminfo
from scipy import sparse
import subprocess as subp
import matplotlib.pyplot as plt

def circleProduct(s,z, stype='R+'):
    if stype=='R+':
        return np.multiply(s,z)
    else:
        raise AssertionError('Given type:' + stype + ' is not supported')

def diamondProduct(s,z, stype='R+'):
    return circleProduct(np.divide(np.ones([s.shape[0], 1]), s), z, stype)


def NTScalingPoint(s,z,stype='R+'):
    return circleProduct(np.power(s, 0.5), np.power(z,-0.5), stype)

def barrier(w, stype='R+'):
    # Only non-negative orthant for now
    return np.sum(np.log(w))

def barrierGradient(w, stype='R+'):
    # Only non-negative orthant for now
    return -np.power(w, -1);

def barrierHessian(w, stype='R+'):
    return np.diag(np.power(w, -2).A1)



def solveLinAlg(A,b):
    return np.linalg.solve(A,b)
    
def solveLinear(A, b):
    # Write A to /dev/shm
    print A.shape
    sA = sparse.csr_matrix(A)
    mmwrite('/dev/shm/A.mtx', sA)
    mmwrite('/dev/shm/B.mtx', b)
    subp.call('/data-local/sykim/SuiteSparse/CHOLMOD/Demo/cholmod_test',shell=True)
    X = np.matrix(mmread('/dev/shm/X.mtx'))
    exit(0)
    # print X
    # X =  np.linalg.solve(A, b) 
    # print Xold
    return X



def primal_dual_solve_nonneg_orthant(P, c, A, b, G, h):
    # Find initial feasible point
    # Assert P is positive semidefinite
    rP, cP = P.shape
    rA, cA = A.shape
    rG, cG = G.shape
    rc = c.shape[0]        
    rb = b.shape[0]
    rh = h.shape[0]
    print c.shape
    print b.shape
    print h.shape
    # Checks
    if rP != cP or cP != cA or cP != cG:
        raise AssertionError('P A, and G should have same number of columns, and q, b and h should be of same length')
    if rP != cP:
        raise AssertionError('P is not square');
    if not rc == rP:
        raise AssertionError('length of c not matching length of P')
    if not rA == rb:
        raise AssertionError('A and b dimension mismatch')
    if not rG == rh:
        raise AssertionError('G and h dimension mismatch')
    eigs, vecs = np.linalg.eig(P)
    for eig in eigs:
        if eig < 0:
            print eigs
            raise AssertionError('P is not positive semidefinite')
    if np.linalg.matrix_rank(A) < rA:
        print np.linalg.matrix_rank(A), rA
        raise AssertionError('A matrix has rank deficiency')
    
    # Make the baseLHS
    rLHS = rP + rG + rA
    cLHS = rP + rG + rA
    baseLHS = np.zeros([rLHS, cLHS])
    baseLHS[0:rP,0:cP] = P;
    baseLHS[0:rP, cP:cP + rA] = np.transpose(A)
    baseLHS[0:rP, cP + rA: cP + rA + rG] = np.transpose(G)
    baseLHS[rP:rP + rA, 0: cA] = A
    baseLHS[rP + rA: rP + rA + rG, 0:cG] = G
    m = rA;
    
    
    checkLHS = np.copy(baseLHS)
    checkLHS[rP:rP + rA, 0: cA] = -A
    checkLHS[rP + rA: rP + rA + rG, 0:cG] = -G
    ##################################################################################
    # Initialize the central path
    initLHS = np.copy(baseLHS)
    initLHS[rP + rA: rP + rA + rG, 0:cG] = -G
    initLHS[rP + rA: rP + rA + rG, rP + rA: rP + rA + rG] = np.identity(rG)
    initRHS = np.concatenate((-c, b, -h), axis=0)
    # Solve initLHS X = initRHS
    initX  = solveLinAlg(initLHS, initRHS)
    initx  = initX[0:rP]
    inity  = initX[rP:rP + rA]
    initz  = initX[rP + rA:]
    alphaP = -(-initz).min()
    alphaD = -initz.min()
    xcap = initx
    ycap = inity

    if alphaD < 0:
        zcap = initz
    else:
        zcap = initz + (1 + alphaD)*np.ones([initz.shape[0],1])
    
    if alphaP < 0:
        scap = -initz
    else:
        scap = -initz + (1  + alphaP)*np.ones([initz.shape[0],1])
    
        
    iter = 0
    # print 'scap'
    # print scap 
    # print 'zcap'
    # print zcap
    # print 'xcap'
    # print xcap
    # print 'ycap'
    # print ycap
    res = -(np.dot(np.dot(np.transpose(xcap), P), xcap) + np.dot(np.transpose(c), xcap)).item(0)
    results = [res]
    print 'iter:', iter, 'result:', res 

    #################################################################################
    w = NTScalingPoint(scap, zcap)
    lambdaV = circleProduct(np.power(zcap, 0.5), np.power(scap, 0.5))
    nIter = 20
    error_margin = 0.001;
    iter = 1

    while (True):
        if (iter >= nIter):
            break
        ############################################################################# 
        # Optimality Condition Check:
        X = np.concatenate((xcap, ycap, zcap), axis = 0)
        S = np.concatenate((np.zeros([rP, 1]), np.zeros([rA, 1]), scap), axis = 0)
        K = np.concatenate((c,b,h), axis=0)
        t1 = np.dot(checkLHS, X) + K
        # Check if approximately optimal
        if (np.linalg.norm(S-t1) < error_margin) and (
                abs(np.dot(np.transpose(zcap),scap)) < error_margin) and (
                    np.linalg.norm(zcap) < error_margin) and (
                        np.linalg.norm(scap) < error_margin):
            print 'Optimality reached'
            break
        
        w = NTScalingPoint(scap, zcap)
        lambdaV = circleProduct(np.power(zcap, 0.5), np.power(scap, 0.5))
    
        ############################################################################
        # Step 1: Compute residuals, 
        # Compute the resuiduals
        rK = np.concatenate((c, -b, -h), axis=0)
        
        R = S + np.dot(baseLHS, X) + rK
        ############################################################################
        # Compute mu
        mu = np.dot(np.transpose(scap),zcap)/ m
        # Its a scalar.. numpy crap
        mu = mu.item(0)
        
        ############################################################################
        # Compute Nesterov Todd Scaling point
        ############################################################################
        # Compute W
        
        W = np.diag(w.A1)
        ############################################################################
        rs_affine = -circleProduct(lambdaV, lambdaV)
        ############################################################################
        # Step 2: Affine direction
        # Compute LHS for steps 2 and 4 
        LHS = np.copy(baseLHS)
        LHS[rP + rA: rP + rA + rG, rP + rA: rP + rA + rG] = -np.transpose(W) * W
        # Compute RHS for step 2
        ds_affine = -circleProduct(lambdaV, lambdaV)
        
        affineRHS = - np.copy(R)
        affineRHS[rP + rA: rP + rA + rG] = affineRHS[rP + rA: rP + rA + rG] - np.dot(np.transpose(W), diamondProduct(lambdaV, ds_affine))

        # Solve for step 2:
        delX_affine = solveLinear(LHS, affineRHS)
        delx_affine = delX_affine[0:rP]
        dely_affine = delX_affine[rP:rP + rA]
        delz_affine = delX_affine[rP + rA: rP + rA + rG]
        dels_affine = np.dot(np.transpose(W) , (diamondProduct(lambdaV, ds_affine) - np.dot(W, delz_affine)))
        # print delx_affine
        # print dely_affine
        ###########################################################################
        # Step 3: Step size and compute parameter:
        # Find alpha
        dels_tilde_affine = np.dot(np.transpose(np.linalg.inv(W)), dels_affine)
        delz_tilde_affine = np.dot(W , delz_affine)

        rhok = circleProduct(np.power(lambdaV, -1), dels_tilde_affine)
        sigk = circleProduct(np.power(lambdaV, -1), delz_tilde_affine)
        alpha = max(0, -np.min(rhok), -np.min(sigk))

        if alpha == 0:
            alpha = float('inf')
        else:
            alpha = 1.0 / alpha
        if (alpha == float('inf')):
            rho = alpha
        else:
            numerator   = np.dot(np.transpose(dels_tilde_affine), delz_tilde_affine)
            denominator = np.dot(np.transpose(lambdaV),lambdaV)
            rho = 1 - alpha + (alpha**2 * numerator / denominator)
        
        if not isinstance(rho, float):
            rho = rho.item(0)
        sigma  = (max(0, min(1, rho)))**3
        ############################################################################
        # Step 4: Combined direction 
        eta = 0
        gamma = 1.0
        Rcomb = -(1 - eta) * R
        ds_comb = -circleProduct(lambdaV, lambdaV) - \
                  (gamma * circleProduct(dels_tilde_affine,delz_tilde_affine)) + \
                  (sigma * mu * np.ones([rG, 1]))
        
        # print ds_comb
        # return
        combRHS = np.copy(Rcomb)
        combRHS[rP + rA: rP + rA + rG] = combRHS[rP + rA: rP + rA + rG] - np.dot(np.transpose(W), diamondProduct(lambdaV, ds_comb))
        delX_comb = solveLinear(LHS, combRHS)
        delx_comb = delX_comb[0:rP]
        dely_comb = delX_comb[rP:rP + rA]
        delz_comb = delX_comb[rP + rA: rP + rA + rG]
        dels_comb = np.dot(np.transpose(W) , (diamondProduct(lambdaV, ds_comb) - np.dot(W, delz_comb)))
        
        ###########################################################################
        #  Step 5: Update iterates and scaling matrices
        # Find alpha
        dels_tilde_comb = np.dot(np.transpose(np.linalg.inv(W)), dels_comb) 
        #np.dot(np.transpose(np.power(W,-1)), dels_comb)
        delz_tilde_comb = np.dot(W, delz_comb)
        # H_12 = sp.linalg.sqrtm(gradientHessian(lambdaV)) 
        
        rhok_comb = circleProduct(np.power(lambdaV, -1), dels_tilde_comb)
        sigk_comb = circleProduct(np.power(lambdaV, -1), delz_tilde_comb)
        alpha_comb = max(0, -np.min(rhok_comb), -np.min(sigk_comb))
        if alpha_comb == 0:
            alpha_comb = float('inf')
        else:
            alpha_comb = 1.0 / alpha_comb

        alpha = alpha_comb * 0.9;
        scap =  scap + alpha *dels_comb
        xcap =  xcap + alpha *delx_comb
        ycap =  ycap + alpha *dely_comb
        zcap =  zcap + alpha *delz_comb
        # print 'scap'
        # print scap 
        # print 'zcap'
        # print zcap
        # print 'xcap'
        # print xcap
        # print 'ycap'
        # print ycap
        res = -(np.dot(np.dot(np.transpose(xcap), P), xcap) + np.dot(np.transpose(c), xcap)).item(0)
        results.append(res)

        print 'iter:', iter, 'result:', res
        # Update w and lambda
        # term1 = lambdaV + alpha * dels_tilde_comb
        # term2 = lambdaV + alpha * delz_tilde_comb
        # print term1
        # print term2
        # w = circleProduct(circleProduct(np.power(term1, 0.5), np.power(term2, -0.5)), w)
        # lambdaV = circleProduct(np.power(term1, 0.5), np.power(term2, 0.5))
        # End of iteration
        iter = iter + 1
    
    x = range(iter)
    plt.plot(x, results)
    plt.xlabel('iteration')
    plt.ylabel('Function value')
    plt.title('Function value for n=' + str(n))
    plt.grid(True)
    plt.show()
    return
if __name__ == '__main__':
    n=2500
    M = np.random.rand(n,n)
    # a = mmread('/home/akshayc/Downloads/bcsstk10/bcsstk10.mtx').todense()
    # n = A.shape[0]
    # A = np.matrix([ [ .3, -.4,  -.2,  -.4,  1.3 ],
    #                 [ .6, 1.2, -1.7,   .3,  -.3 ],
    #                 [-.3,  .0,   .6, -1.2, -2.0 ] ])
    # P = np.copy(A)#np.dot(np.transpose(A), A) # + np.identity(n)
    P = np.dot(np.transpose(M), M) + np.identity(n)
    c = np.random.rand(n,1);
    A = np.ones([1,n])

    b = np.matrix(np.ones(1))
    G = -np.identity(n)
    h = np.zeros([n,1])
    # P = np.matrix([[1.0,0,0][0,1.0,3.0], [0,0,1.0]]);
    # c = np.matrix([[0.5],[3],[1]])
    # G = np.matrix([[1.0,0,0][0,1.0,0.0], [0,0,1.0]]);
    # h = np.zeros([3,1])
    # A = 
    primal_dual_solve_nonneg_orthant(P, c, A, b, G, h)
