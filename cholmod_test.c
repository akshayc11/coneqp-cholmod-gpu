#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include "cholmod.h"

int main (void)
{
    struct timeval t1, t2;
    double elapsedTime;

    //const char* matFile = "../Matrix/nd6k/nd6k.mtx";
    const char* matFile = "/dev/shm/A.mtx";
    FILE* fp = fopen(matFile, "r");
    assert(fp != NULL);

    const char* matFile2 = "/dev/shm/B.mtx";
    FILE* fp2 = fopen(matFile2, "r");
    FILE* fp3;
    assert(fp2 != NULL);
    SuiteSparse_long sym ;
    char comments [1000] ;

    cholmod_sparse *A ;
    cholmod_dense *x, *b;
    cholmod_factor *L ;

    cholmod_common* c = (cholmod_common*)malloc(sizeof(cholmod_common));
    cholmod_l_start (c) ; /* start CHOLMOD */
    c->useGPU = 1;
    c->supernodal = CHOLMOD_SUPERNODAL;

    A = cholmod_l_read_sparse (fp, c) ; /* read in a matrix */
    //cholmod_l_print_sparse (A, "A", c) ; /* print the matrix */
    fclose(fp);

    //if (A == NULL || A->stype == 0) /* A must be symmetric */
    //{
    //    cholmod_l_free_sparse (&A, c) ;
    //    cholmod_l_finish (c) ;
    //    return (0) ;
    //}
    
    b = cholmod_l_read_dense (fp2, c) ; 
    //b = cholmod_l_ones (A->nrow, 1, A->xtype, c) ; /* b = ones(n,1) */
    fclose(fp2);

    gettimeofday(&t1, NULL);
    L = cholmod_l_analyze (A, c) ; /* analyze */
    cholmod_l_factorize (A, L, c) ; /* factorize */
    x = cholmod_l_solve (CHOLMOD_A, L, b, c) ; /* solve Ax=b */
    gettimeofday(&t2, NULL);
    elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
    printf("Time: %.4f ms\n", elapsedTime);
    cholmod_l_gpu_stats(c);

    /* ---------------------------------------------------------------------- */
    /* write X matrices  */
    /* ---------------------------------------------------------------------- */
    comments [0] = '\0' ;
    if ((fp3 = fopen ("/dev/shm/X.mtx", "w")) == NULL)
    {
        printf ("error opening file") ;
    }
    sym = cholmod_l_write_dense (fp3, x, comments, c) ;
    fclose (fp3) ;
    if (sym < 0)
    {
        printf ("mwrite failed") ;
    }

    cholmod_l_free_factor (&L, c) ; /* free matrices */
    cholmod_l_free_sparse (&A, c) ;
    cholmod_l_free_dense (&x, c) ;
    cholmod_l_free_dense (&b, c) ;
    cholmod_l_finish (c) ; /* finish CHOLMOD */
    return (0) ;
}
